import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import fsolve, minimize

#Уравнения Лотки-Вольтерры - предоставленный класс Biome

class Biome(object):

  def __init__(self, pred_init=1, prey_init=1, tmax=10, dt=0.001):
    self._dt = dt
    self._n_iters = int(tmax / dt)
    # Intial state
    self.pred_init = pred_init
    self.prey_init = prey_init
    # Data arrays
    self.time = None
    self.prey = None
    self.pred = None

  def run(self, alpha, beta, delta, gamma):
    """
    Simulation settings:
        alpha - prey growth
        beta - prey mortality
        delta - predator growrh
        gamma - predator mortality
    """
    self.time = np.zeros(self._n_iters)
    self.prey = np.zeros(self._n_iters)
    self.pred = np.zeros(self._n_iters)
    self.prey[0] = self.prey_init
    self.pred[0] = self.pred_init
    # Lotka-Volterra equations
    for i in range(self._n_iters - 1):
      self.prey[i + 1] = self.prey[i] + self._dt * self.prey[i] * (alpha - self.pred[i] * beta)
      self.pred[i + 1] = self.pred[i] + self._dt * self.pred[i] * (delta * self.prey[i] - gamma)
    self.time[1:] = np.add.accumulate([self._dt] * (self._n_iters - 1))
    return self.time, self.prey, self.pred

  def plot(self):
    plt.xlabel('Time')
    plt.ylabel('N')
    plt.plot(self.time, self.pred, label='Predators', color='black')
    plt.plot(self.time, self.prey, label='Preys', color='yellow')
    plt.legend(loc="center", bbox_to_anchor=(0.5, 1.1))
    plt.grid()
    plt.show()

  def plot_cycle(self):
    plt.xlabel('N predators')
    plt.ylabel('N preys')
    plt.plot(self.pred, self.prey, color='yellow')
    plt.grid()
    plt.show()

#Критерий устойчивости - отношение числа хищников к числу жертв, когда близко вымирание, должно быть равно 1.3

def calc_count(coeff):
    alpha, beta, gamma, delta = coeff

    x0 = 1
    y0 = 1
    n = 1.3

#Стационарная точка

    xstat = gamma/delta
    ystat = alpha/beta

#Описанные ниже три выражения получаются из уравнений Лотки-Вольтерры. С помощью них определяются численности и хищников, и жертв на пике.

    c = delta*x0 + beta*y0 - gamma*np.log(np.abs(x0)) - alpha*np.log(np.abs(y0))

    def x_new(x):
        return delta*x + beta*ystat - gamma*np.log(np.abs(x)) - alpha*np.log(np.abs(ystat)) - c

    def y_new(y):
        return delta*xstat + beta*y - gamma*np.log(np.abs(xstat)) - alpha*np.log(np.abs(y)) - c

#х1, у1 - численность жертв и хищников на грани вымирания

    x1 = fsolve(x_new, 0.001)
    y1 = fsolve(y_new, 0.001)

    p = np.abs(y1/x1-n)
    return p

def main():
    x0 = 1
    y0 = 1
    solution = minimize(calc_count, [1,1,1,1])
    alpha, beta, gamma, delta = solution.x
    print ('alpha =',alpha,'\nbeta = ',beta,'\ngamma =',gamma, '\ndelta =',delta)
    biome = Biome(pred_init = y0, prey_init = x0, tmax=10, dt = 0.001)
    biome.run(alpha, beta, gamma, delta)
    biome.plot()
    biome.plot_cycle()

if __name__ == "__main__":
    main()